
import 'package:flutter/material.dart';

import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'Memorija.dart';

class TestPage extends StatefulWidget {
  TestPage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  //final String title;

  

  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {

  int _score = 0;

  //TODO: FIXAT DA SE ODMAH UCITA NOVA
  var trenutnaRijec = "pocetak";

  var rezultatRijeci = [];

  var listaRijeci = [];

    
  int rbr = 0;


  Firestore firestore = Firestore.instance;

  Memorija memorija = new Memorija();



   @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    ucitajRedniBroj();

    loadAsset();
    
  }

  void ucitajRedniBroj() async{

    rbr = await memorija.readZadnjiRedniBrojRijeci();
    print("ucitao: " + rbr.toString());
  }

  void spremiRedniBroj() {
    
    memorija.saveZadnjiRedniBrojRijeci(rbr);
    //Za resetiranje na 0 ako je potrebno
   // memorija.saveZadnjiRedniBrojRijeci(0);

    print("Spremam:" + rbr.toString());
  }

  void getDataLagane() {
    firestore
        .collection("Lagane")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) => print('${f.data["Rijec"]}}'));
    });
  }
  void getDataSrednje() {
    firestore
        .collection("Srednje")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) => print('${f.data["Rijec"]}}'));
    });
  }
  void getDataTeske() {
    firestore
        .collection("Teske")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) => print('${f.data["Rijec"]}}'));
    });
  }

  void izbaci(String rijec) async {

    await firestore.collection("Izbaci")
        .add({
          'Rijec': rijec,
        });
  }
  void dodajLaganu(String rijec) async {

    await firestore.collection("Lagane")
        .add({
          'Rijec': rijec,
        });
  }
  void dodajSrednju(String rijec) async {

    await firestore.collection("Srednje")
        .add({
          'Rijec': rijec,
        });
  }

  void dodajTesku(String rijec) async {

      await firestore.collection("Teske")
          .add({
            'Rijec': rijec,
          });
    }




  
 
  
  void incrementCounter() {
    setState(() {
      
      _score++;
    });
  }

  

  void loadAsset() async {
    print("loadAssets");

    String lista =  await rootBundle.loadString('assets/AliasRijeci.txt');
    listaRijeci = lista.split("\n");

    setState(() {
        trenutnaRijec = novaRijec();

    });

  }
  String novaRijec()  {
    int x = rbr;

    setState(() {
      
  
    Random random = new Random();
    int randomBroj = random.nextInt(2999);
     // print(listaRijeci[randomBroj]);

     // return listaRijeci[randomBroj];
     spremiRedniBroj();
     rbr++;
     
     
    });
    return listaRijeci[x];

  }
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      
      body: Center(
       
        child: Stack(
         
          //mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 20,left: 0,right: 20), 
                child: Align(
                  alignment: Alignment.topRight,
                  child: Text(
                  '$_score',
                  style: Theme.of(context).textTheme.display1,
                ),
              ),
            ),
            /* Padding(
                padding: EdgeInsets.only(top: 20,left: 20,right: 0), 
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                  '$_start',
                  style: Theme.of(context).textTheme.display1,
                ),
              ),
            ),*/
             Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 50,left: 00,right: 0),
                child: RaisedButton(
                        color: Colors.red,
 
                        onPressed: () {
                           izbaci(trenutnaRijec);
                           trenutnaRijec = novaRijec();

                        },
                        child: Icon(
                          
                          Icons.close,
                          color: Colors.white,
                        ),
                      ),
              ), 
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
              trenutnaRijec,
              style: Theme.of(context).textTheme.display1,
              ),
            ),
           
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
              padding: EdgeInsets.only(bottom: 50,left: 0,right: 0),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:<Widget>[

                    //LAGANA
                    Expanded(

                      child: RaisedButton(
                        color: Colors.white,
 
                        onPressed: () {
                           dodajLaganu(trenutnaRijec);
                           trenutnaRijec = novaRijec();

                        },
                        child: Text("Lagana"),
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    //Srednja
                    Expanded(

                      child: RaisedButton(
                        color: Colors.white,
 
                        onPressed: () {
                          //getData();
                          dodajSrednju(trenutnaRijec);
                          trenutnaRijec = novaRijec();
                        },
                        child: Text("Srednja"),
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Expanded(

                      child: RaisedButton(
                        color: Colors.white,
 
                        //Teska
                        onPressed: () {
                          setState(() {
                              dodajTesku(trenutnaRijec);
                              trenutnaRijec = novaRijec();

                          });
                         
                        },
                        child: Text("Teska"),
                      ),
                    ),

                   ],
                ),

             ),
           ),
          ],
        ),
      ),
      /*floatingActionButton: FloatingActionButton(
        onPressed: incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),*/ // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


