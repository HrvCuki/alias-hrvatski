
import 'package:shared_preferences/shared_preferences.dart';


class Memorija {




  Future<int> read() async {
        final prefs = await SharedPreferences.getInstance();
        final key = 'vrijeme_key';
        final value = prefs.getInt(key) ?? 60;
        return value;
  }

  Future<int> save(int value) async {
        final prefs = await SharedPreferences.getInstance();
        final key = 'vrijeme_key';
        prefs.setInt(key, value);
        return value;
  }

  Future<int> readZadnjiRedniBrojRijeci() async {
        final prefs = await SharedPreferences.getInstance();
        final key = 'redniBroj_key';
        final value = prefs.getInt(key) ?? 0;
        return value;
  }

  Future<int> saveZadnjiRedniBrojRijeci(int value) async {
        final prefs = await SharedPreferences.getInstance();
        final key = 'redniBroj_key';
        prefs.setInt(key, value);
        return value;
  }
}