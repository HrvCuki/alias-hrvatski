import 'package:Alias/GamePage.dart';
import 'package:Alias/TestPage.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'Memorija.dart';
class TeamsPage extends StatefulWidget {
  @override
  _TeamsPageState createState() => new _TeamsPageState();
}

class _TeamsPageState extends State<TeamsPage> {


  final teamController1 = TextEditingController();
  final igrac1Controller1 = TextEditingController();
  final igrac2Controller1 = TextEditingController();

  final teamController2 = TextEditingController();
  final igrac1Controller2 = TextEditingController();
  final igrac2Controller2 = TextEditingController();

  final teamController3 = TextEditingController();
  final igrac1Controller3 = TextEditingController();
  final igrac2Controller3 = TextEditingController();

  final teamController4 = TextEditingController();
  final igrac1Controller4 = TextEditingController();
  final igrac2Controller4 = TextEditingController();

  var teamControllers = List<TextEditingController>();
  var igrac1Controllers = List<TextEditingController>();
  var igrac2Controllers = List<TextEditingController>();
 // Memorija memorija = new Memorija();
  
  var _rows = List<DataRow>();

  int brojRedova = 0;

  List<Tim> timovi = List<Tim>();

  SharedPref sharedPref = SharedPref();

  Tim tim1 = new Tim();
  Tim tim2 = new Tim();
  Tim tim3 = new Tim();
  Tim tim4 = new Tim();

 
  loadSharedPrefs() async {
    try {
      Tim loadTim1 = Tim.fromJson(await sharedPref.read("tim1"));
      Tim loadTim2 = Tim.fromJson(await sharedPref.read("tim2"));
      Tim loadTim3 = Tim.fromJson(await sharedPref.read("tim3"));
      Tim loadTim4 = Tim.fromJson(await sharedPref.read("tim4"));

      setState(() {
        tim1 = loadTim1;
        tim2 = loadTim2;
        tim3 = loadTim3;
        tim4 = loadTim4;

        timovi.add(tim1);
        timovi.add(tim2);
        timovi.add(tim3);
        timovi.add(tim4);

        ucitajTimove();

      });
    } catch (Excepetion) {
        print("nema niceg");
    }
  }

  
  void dodajController(){
      
      teamControllers.add(teamController1);
      teamControllers.add(teamController2);
      teamControllers.add(teamController3);
      teamControllers.add(teamController4);

      igrac1Controllers.add(igrac1Controller1);
      igrac1Controllers.add(igrac1Controller2);
      igrac1Controllers.add(igrac1Controller3);
      igrac1Controllers.add(igrac1Controller4);

      igrac2Controllers.add(igrac2Controller1);
      igrac2Controllers.add(igrac2Controller2);
      igrac2Controllers.add(igrac2Controller3);
      igrac2Controllers.add(igrac2Controller4);

      
  }

  void ucitajTimove(){
    for (var i = 0; i < 4; i++){
      if (timovi[i].tim != null && timovi[i].igrac1 != null && timovi[i].igrac2 != null){
        
        teamControllers[i].text = timovi[i].tim;
        igrac1Controllers[i].text = timovi[i].igrac1;
        igrac2Controllers[i].text = timovi[i].igrac2;
       
        

      }
    }
    

  }

  void dodajRedove()async{
    var brRedova = await sharedPref.readBroj("brojRedova");
    print(brRedova);

    for (var i = 0; i < brRedova; i++){
      dodajRed();
    }
  }

  void izbrisiRed(){
    if(brojRedova > 1){
      setState(() { 
        _rows.removeLast();

        int x = brojRedova - 1;
        timovi[x].tim = "";
        timovi[x].igrac1 = "";
        timovi[x].igrac2 = "";

        sharedPref.save("tim" + brojRedova.toString(), timovi[x].toJson());

        brojRedova--;
        sharedPref.saveBroj("brojRedova", brojRedova);
          
        teamControllers[x].text = "";
        igrac1Controllers[x].text = "";
        igrac2Controllers[x].text = "";
      
      });
    }else {
        timovi[0].tim = "";
        timovi[0].igrac1 = "";
        timovi[0].igrac2 = "";

        sharedPref.save("tim" + brojRedova.toString(), timovi[0].toJson());
          
        teamControllers[0].text = "";
        igrac1Controllers[0].text = "";
        igrac2Controllers[0].text = "";
      
    }
   
  }
  void dodajRed() {
        _rows.add(DataRow(
        
          cells: [
            DataCell(    
              TextField(
                controller: teamControllers[brojRedova],
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Ime tima'
              ),
              )
            ),
            DataCell(
              TextField(
                controller: igrac1Controllers[brojRedova],
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Ime igrača'
              ),
              )
            ),
            DataCell(
              TextField(
                controller: igrac2Controllers[brojRedova],
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Ime igrača'
              ),
              )
            ),
          ],
        ));
      
    
      brojRedova++;
      sharedPref.saveBroj("brojRedova", brojRedova);
    
  }

  
  @override
  void initState() {

    super.initState();
    dodajController();
    dodajRedove();
    loadSharedPrefs();

    


    
    


    
  }


 
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Teams'),
        
      ),body: Stack(
        children: <Widget>[

          Container(
       // child: SingleChildScrollView(
         // scrollDirection: Axis.horizontal,
            
          child: SizedBox.expand(
              child: DataTable(

                columns: [
                  DataColumn(label: Text(
                    
                    "Tim: ",
                     style: TextStyle(fontWeight: FontWeight.bold),

                    )
                  
                  ),
                  DataColumn(label: Text("Igrač 1: ")),
                  DataColumn(label: Text("Igrač 2: ")),
                ],

              rows: _rows,
              ),
          ),
        ),

          Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 50,left: 10,right: 10),

                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Center(
                        child: RaisedButton(
                        child: Text("PLAY"),
                        onPressed: (){

                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => GamePage()),

                          );     
                        },
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          child: Text("Done"),
                          onPressed: () {
                              tim1.tim = teamControllers[0].text;
                              tim1.igrac1 = igrac1Controllers[0].text;
                              tim1.igrac2 = igrac2Controllers[0].text;

                              tim2.tim = teamControllers[1].text;
                              tim2.igrac1 = igrac1Controllers[1].text;
                              tim2.igrac2 = igrac2Controllers[1].text;

                              tim3.tim = teamControllers[2].text;
                              tim3.igrac1 = igrac1Controllers[2].text;
                              tim3.igrac2 = igrac2Controllers[2].text;

                              tim4.tim = teamControllers[3].text;
                              tim4.igrac1 = igrac1Controllers[3].text;
                              tim4.igrac2 = igrac2Controllers[3].text;

                              
                              sharedPref.save("tim1", tim1.toJson());
                              sharedPref.save("tim2", tim2.toJson());
                              sharedPref.save("tim3", tim3.toJson());
                              sharedPref.save("tim4", tim4.toJson());

                        
                              print("savam");
                              
        
                          
                          },
                        ),
                        SizedBox(width: 20,),
                        RaisedButton(
                          child: Text("Dodaj tim"),
                          onPressed: (){
                        
                            if(brojRedova < 4){
                              setState(() {
                              dodajRed();
                              });
                            }else {
                              print("Max timova");
                            }
                          
                          },
                        ),
                        SizedBox(width: 20,),
                        RaisedButton(
                          child: Text("Izbriši tim"),
                          onPressed: (){
                        
                          
                              izbrisiRed();
                            
                          
                            
                          
                          },
                        ),
                      ],
                    ),
                  ],
                    
                  
                ),
                  
                  
                   
              ),
          ),
        ],
       ),  
    );
    
  }
}

class Tim {

  String tim;
  String igrac1;
  String igrac2;

  int score;

  Tim();


  Tim.fromJson(Map<String, dynamic> json) 
      : tim = json['tim'],
        igrac1 = json['igrac1'],
        igrac2 = json['igrac2'];

  Map<String, dynamic> toJson() => {
    'tim': tim,
    'igrac1': igrac1,
    'igrac2': igrac2,
  };

}

class SharedPref {
  read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key));
  }

  save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

   Future<int> readBroj(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key) ?? 0;
  }

  saveBroj(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

}