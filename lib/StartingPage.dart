import 'package:Alias/GamePage.dart';
import 'package:Alias/SettingsPage.dart';
import 'package:Alias/TeamsPage.dart';
import 'package:Alias/TestPage.dart';
import 'package:flutter/material.dart';

import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class StartingPage extends StatefulWidget {
  @override
  _StartingPageState createState() => new _StartingPageState();
}

class _StartingPageState extends State<StartingPage> {





  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Main Menu'),
        
      ),body: Stack(
        children: <Widget>[
          
          
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("facebook sign in"),
                  onPressed: () async{
                      final facebookLogin = FacebookLogin();
                      final result = await facebookLogin.logIn(['email']);

                        switch (result.status) {
                          case FacebookLoginStatus.loggedIn:
                            print(result.accessToken.token);
                            //_sendTokenToServer(result.accessToken.token);
                            //_showLoggedInUI();
                            break;
                          case FacebookLoginStatus.cancelledByUser:
                             print("cancel");

                           // _showCancelledMessage();
                            break;
                          case FacebookLoginStatus.error:
                             print(result.errorMessage);
                            //_showErrorOnUI(result.errorMessage);
                            break;
                        }
                   
                  },
                ),
                 RaisedButton(
                  child: Text("Teams"),
                  onPressed: (){

                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TeamsPage()),

                    );     
                  },
                ),
                RaisedButton(
                  child: Text("PLAY"),
                  onPressed: (){

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => GamePage()),

                    );     
                  },
                ),
              ],
            ), 
          ),
           Padding(
              padding: EdgeInsets.only(bottom: 50,left: 50,right: 50),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  child: Text("RAZVRSTAJ"),
                  onPressed: (){

                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TestPage()),

                    );     
                },
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 50,left: 50,right: 50),
              child: Align(
                alignment: Alignment.topCenter,
                child: RaisedButton(
                  child: Text("Settings"),
                  onPressed: (){

                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SettingsPage()),

                    );     
                },
              ),
            ),
          ),


        ],
      )
    );
  }
}
