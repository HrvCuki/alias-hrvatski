import 'package:Alias/GamePage.dart';
import 'package:Alias/TestPage.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'Memorija.dart';
class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => new _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {



  Memorija memorija = new Memorija();

  int vrijeme;

  @override
  void initState() {
    ucitajVrijeme();

    super.initState();
  
    ucitajVrijeme();

    
  }


  void ucitajVrijeme() async {
    
    vrijeme = await memorija.read();
    setState(() {
      
    });
    print(vrijeme);

  }

  void zapisiVrijeme(int novoVrijeme) async {

    vrijeme = await memorija.save(novoVrijeme);
    
    print(vrijeme);

  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Settings'),
        
      ),body: Stack(
        children: <Widget>[

          Align(
              alignment: Alignment.center,
              child: Padding(
              padding: EdgeInsets.only(bottom: 50,left: 10,right: 10),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:<Widget>[

                    //30
                    Expanded(

                      child: RaisedButton(
                        color: vrijeme == 30 ? Colors.grey : Colors.white,
 
                        onPressed: () {
                          setState(() {
                              if (vrijeme != 30){
                                zapisiVrijeme(30);

                              }
                          });
                          print(vrijeme);

                        },
                        child: Text("30 s"),
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    //60
                    Expanded(

                      child: RaisedButton(
                        
                        color: vrijeme == 60 ? Colors.grey : Colors.white,
 
                        onPressed: () {

                          setState(() {
                              if (vrijeme != 60){
                                zapisiVrijeme(60);

                              }
                          });

                          //getData();
                         
                        },
                        child: Text("60 s"),
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Expanded(

                      child: RaisedButton(
                        color: vrijeme == 90 ? Colors.grey : Colors.white,
 
                        //90 s
                        onPressed: () {
                          setState(() {
                              if (vrijeme != 90){
                                zapisiVrijeme(90);

                              }
                          });
                         
                        },
                        child: Text("90 s"),
                      ),
                    ),

                   ],
                ),

             ),
           ),

          Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 50,left: 10,right: 10),

                child: RaisedButton(
                  child: Text("Done"),
                  onPressed: (){
                      Navigator.pop(context);

                  },
                ),
              ),
          ),
        ],
       ),  
    );
    
  }
}
