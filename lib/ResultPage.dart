//import 'dart:html';

import 'package:flutter/material.dart';
import './GamePage.dart';
import 'TeamsPage.dart';

class ResultPage extends StatefulWidget {

  Score score;

  ResultPage(Score score){
    this.score = score;

  }
  
  @override
  _ResultPageState createState() => new _ResultPageState(score);
}

class _ResultPageState extends State<ResultPage> {

  Score score;

  int ukupno;
  int tocno;
  int krivo;
  _ResultPageState(Score score){
    this.score = score;
    this.ukupno = score.score;

  }
  
  var a = [1,2,3];
  var _rows = List<DataRow>();

  var timovi;

  void dodajRijeci(){

    int tocnoTmp = 0;
    int krivoTmp = 0;

    for (var i in score.listaRijeci){
      var ikona;
      if (i[1] == 1){
        tocnoTmp++;
        ikona = Icon(
          Icons.check,
          color: Colors.green
        );
      }else if (i[1] == -1){
        krivoTmp++;
        ikona = Icon(
          Icons.close,
          color: Colors.red
        );

      }
      _rows.add(DataRow(
        cells: [
          DataCell(Text(i[0].toString())),
          DataCell(ikona),
        ],
      ));
    }

    tocno = tocnoTmp;
    krivo = krivoTmp;
  }

   @override
  void initState() {
    super.initState();
    dodajRijeci();
    
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Rezultati'),
        
      ),
      body: Stack(

        children: <Widget>[

          Container(
       // child: SingleChildScrollView(
         // scrollDirection: Axis.horizontal,
            
          child: SizedBox.expand(
              child: DataTable(

                columns: [
                  DataColumn(label: Text(
                    
                    "Ukupno: $ukupno",
                     style: TextStyle(fontWeight: FontWeight.bold),

                    )
                  
                  ),
                  DataColumn(label: Text("Točno: $tocno" + "\n\n Krivo: $krivo")),

                ],
                  
                  

                 

              rows: _rows,
              ),
          ),
        ),

          Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
              padding: EdgeInsets.only(bottom: 50,left: 50,right: 50),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:<Widget>[

                   // Expanded(

                      /*child: RaisedButton(
                        color: Colors.white,
                        //TOCNO
                        onPressed: () {
   
                        },
                        child: Icon(
                          Icons.check,
                          size: 50,
                          color: Colors.green,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),*/
                    //NETOCNO
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                         RaisedButton(
                          
                          color: Colors.blue,
  
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) => GamePage()),
                            );     

                          },
                          child: Text(
                            "Igraj",
                            style: TextStyle(color: Colors.white),
                          ),

                        ),
                        Text("Sljedeči tim: "),
                      
                      ],
                      
                    ),

                   ],
                ),

             ),
           ),
        ],
      ),
      
      
     // ),
    );
  }
}


