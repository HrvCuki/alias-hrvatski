import 'package:flutter/material.dart';

import './GamePage.dart';
import './StartingPage.dart';
import './ResultPage.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',

      initialRoute: '/mainMenu',
      //initialRoute: loggedIn ? '/' : '/login',
      routes: {
        '/mainMenu': (context) => new StartingPage(),

        '/game': (context) => new GamePage(),

        '/results': (context) => new ResultPage(new Score(0, new List())),

      },
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
      ),
    );
  }
}

