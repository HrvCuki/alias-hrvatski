
import 'package:Alias/TeamsPage.dart';
import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

import 'dart:math';

import './ResultPage.dart';
import 'Memorija.dart';

class GamePage extends StatefulWidget {
  //GamePage({Key key}) : super(key: key);

  /*List<Tim> listaTimova;
  GamePage(List<Tim> listaTimova){
    this.listaTimova = listaTimova;

  }*/

  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {

  int _score = 0;

  //TODO: FIXAT DA SE ODMAH UCITA NOVA
  var trenutnaRijec = "pocetak";

  var rezultatRijeci = [];

  var listaRijeci = [];

  Memorija memorija = new Memorija();

  Timer _timer;
  int _start = 3;

  void ucitajVrijeme() async {
    
    _start = await memorija.read();
    setState(() {
      
    });

  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {

            //TODO: DA SE NEMRE VISE VRATT NA STARU IGRU
           // Navigator.pushNamed(context, "/results");
            var rezultat = new Score(_score, rezultatRijeci);

            
           Navigator.pushReplacement(
            context,
           MaterialPageRoute(builder: (context) => ResultPage(rezultat)),
            );
           
           // Navigator.pop(lastContext);
            print("se ovo opce des");
            //print(rezultat.score);
           // print(rezultat.listaRijeci);


            timer.cancel();
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }
  @override
  void dispose() {

    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    
    //ucitajVrijeme();
    startTimer();
    loadAsset();
    
  }
  
  void _incrementCounter() {
    setState(() {
      
      _score++;
    });
  }

  void procitaj(){

    final file = new File('/assets/AliasRijeci.txt');
    Stream<List<int>> inputStream = file.openRead();

    inputStream
    .transform(utf8.decoder)       // Decode bytes to UTF-8.
    .transform(new LineSplitter()) // Convert stream to individual lines.
    .listen((String line) {        // Process results.
        print('$line: ${line.length} bytes');
      },
      onDone: () { print('File is now closed.'); },
      onError: (e) { print(e.toString()); });


  }

  Future<String> loadAsset() async {
    print("loadAssets");

    String lista =  await rootBundle.loadString('assets/AliasRijeci.txt');
    listaRijeci = lista.split("\n");

    setState(() {
        trenutnaRijec = novaRijec();

    });

  }

  String novaRijec()  {

      Random random = new Random();
      int randomBroj = random.nextInt(2999);
      print(listaRijeci[randomBroj]);

      return listaRijeci[randomBroj];



  }
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      
      body: Center(
       
        child: Stack(
         
          //mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 20,left: 0,right: 20), 
                child: Align(
                  alignment: Alignment.topRight,
                  child: Text(
                  '$_score',
                  style: Theme.of(context).textTheme.display1,
                ),
              ),
            ),
             Padding(
                padding: EdgeInsets.only(top: 20,left: 20,right: 0), 
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                  '$_start',
                  style: Theme.of(context).textTheme.display1,
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
              trenutnaRijec,
              style: Theme.of(context).textTheme.display1,
              ),
            ),
           
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
              padding: EdgeInsets.only(bottom: 50,left: 50,right: 50),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:<Widget>[

                    //TOCNO
                    Expanded(

                      child: RaisedButton(
                        color: Colors.white,
                        

                        //shape: new CircleBorder(),

                        //TOCNO
                        onPressed: () {
                          setState(() {
                            rezultatRijeci.add([trenutnaRijec, 1]);
                            trenutnaRijec = novaRijec();
                            _score++;
                          });
                          
                          
                        },
                        child: Icon(
                          Icons.check,
                          size: 50,

                          color: Colors.green,
                        ),


                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    //NETOCNO
                    Expanded(
                      child: RaisedButton(

                        color: Colors.white,
                        //shape: new CircleBorder(),
                        //NETOCNO
                        //KRIVO

                        onPressed: () {

                           setState(() {
                            rezultatRijeci.add([trenutnaRijec,-1]);
                            trenutnaRijec = novaRijec();
                            if(_score > 0){
                              _score--;

                            }
                          });

                        },
                        child: Icon(
                          Icons.close,
                          size: 50,

                          color: Colors.red,
                        ),

                      ),
                    ),

                   ],
                ),

             ),
           ),
          ],
        ),
      ),
      /*floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),*/ // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


class Score { 

   int score;
   var listaRijeci;

   Score(int score, var listaRijeci) { 
     this.score = score;
     this.listaRijeci = listaRijeci;
     print(score);
     print(listaRijeci); 
   } 
}